// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyC3OT5PB4JGbZxdrFj7Szqn09nwYiti6fI",
    authDomain: "angularhome-b1fab.firebaseapp.com",
    databaseURL: "https://angularhome-b1fab.firebaseio.com",
    projectId: "angularhome-b1fab",
    storageBucket: "angularhome-b1fab.appspot.com",
    messagingSenderId: "985237463530",
    appId: "1:985237463530:web:02c1c9cf948b4c658a1293",
    measurementId: "G-5DQNGFQB1V"
  } 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
