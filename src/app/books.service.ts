import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
 
  books: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection;
  // addBooks(){
  //   setInterval(() => 
  //     this.books.push({title:'A new one', author:'New author'})
  // , 5000);    
  // }

//   getBooks(): any {
//     const booksObservable = new Observable(observer => {
//            setInterval(() => 
//                observer.next(this.books)
//            , 5000);
//     });

//     return booksObservable;
// }

  /*
  getBooks(){
    setInterval(() => {return this.books}, 1000);
  }
  */

  getBooks(userId):Observable<any[]>{

 //   return this.db.collection('books').valueChanges({idField:'id'}); // את שדה ה-ID תכניס בתכונה שנקראת ID של book
      this.bookCollection = this.db.collection(`users/${userId}/books`)
      return this.bookCollection.snapshotChanges().pipe(
        map(
          collection => collection.map(
            document => {
              const data = document.payload.doc.data();
            data.id = document.payload.doc.id; 
            console.log(data);
            return data;           }
          )
        )
      )
  }

  getBook(id:string,userId:String):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }

  addBook(userId:string,title:String, author:String){

    const book = {title:title, author:author}; // first title - key, second title - value
    this.userCollection.doc(userId).collection('books').add(book)
  //  this.db.collection('books').add(book);

  }

  constructor(private db:AngularFirestore) { 

  }

  deleteBook(id:string,userId:string){

    this.db.doc(`users/${userId}/books/${id}`).delete();

  }

  updateBook(userId:string,id:String,title:String, author:String){

    this.db.doc(`users/${userId}/books/${id}`).update({
      title:title,
      author:author
    })
  }
}