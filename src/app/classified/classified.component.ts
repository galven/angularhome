import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading..";
  categoryImage:string;

  constructor(public ClassifyService:ClassifyService, public imageService:ImageService) { }

  ngOnInit() {
    this.ClassifyService.classify().subscribe(
      res =>{
        this.category = this.ClassifyService.categories[res];
        this.categoryImage = this.imageService.images[res];

      }
    )
  }

}
