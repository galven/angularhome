import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private url = "https://8dlehdqpjh.execute-api.us-east-1.amazonaws.com/beta"

  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  public doc:string;

  classify():Observable<any>{
    let json = {
      "articles":[
        {
          "text":this.doc
        },
      ]
    }
    let body = JSON.stringify(json); // העברת ג'ייסון ע"י סטרינגיפי, בשביל להעביר בפרוטוקול HTTP

    return this.http.post<any>(this.url,body).pipe(
      map(res =>{
        let final = res.body.replace('[','');
        final = final.replace(']','');
        return final;
      })
    );

  }
  

  constructor(private http:HttpClient) { }
}
