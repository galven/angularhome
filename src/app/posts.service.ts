import { BlogPost } from './interfaces/blog-post';
import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BlogUser } from './interfaces/blog-user';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiurl = "https://jsonplaceholder.typicode.com/posts/"
  apiurluser = "https://jsonplaceholder.typicode.com/users"

  constructor(private _http: HttpClient, private db:AngularFirestore) { }

 
  getPost(){
    return this._http.get<BlogPost[]>(this.apiurl);
  }

  getUser(){
    return this._http.get<BlogUser[]>(this.apiurluser);
  }

  addPost(title:String, body:String, username:String){
    const post = {title:title, body:body, username:username}
    this.db.collection('posts').add(post);

  }

  addPosts(posts :BlogPost[],users: BlogUser[]){
    let bpLen = posts.length
    let uLen = users.length
    for(let i = 0; i < bpLen; i++){
        for(let j = 0; j<uLen; j++)
        if(posts[i].userId === users[j].id){
          this.addPost(posts[i].title, posts[i].body, users[j].username)
        }
    }
  }
}

