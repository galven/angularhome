import { User } from './../interfaces/user';
import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, UrlTree, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  buttonText: string = "Add book";
  title:String;
  author:String;
  id:string;
  isEdit: boolean = false;
  userId: string;

  onSubmit(){
    if(this.isEdit){
    this.BooksService.updateBook(this.userId,this.id,this.title,this.author);
    } else{
      this.BooksService.addBook(this.userId,this.title,this.author);
    }
    this.router.navigate(['/books']);
  }

  constructor(private router: Router,private BooksService: BooksService, private route: ActivatedRoute, private AuthService:AuthService) { }

  ngOnInit() {

    this.id = this.route.snapshot.params.id;
    this.AuthService.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id) {
          this.isEdit = true;
          this.buttonText = "Update Book";
          this.BooksService.getBook(this.id,this.userId).subscribe(
        
            book => {
              this.author = book.author;
              this.title = book.title;
            }
      )}
      })
  }

}
