import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  panelOpenState = false;
 // books: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
 books$:Observable<any>;
 userId: string;

 constructor(private bookservice:BooksService, public AuthService:AuthService) { }

 deleteBook(id:string){
  this.bookservice.deleteBook(id, this.userId);
 }

  ngOnInit() {

       //this.books = this.bookservice.getBooks();
    /*
    this.bookservice.getBooks().subscribe(
      (books) => {this.books = books}
    )
    */

   // this.bookservice.addBooks();
 //   this.books$ = this.bookservice.getBooks();
   
   this.AuthService.user.subscribe(
     user => {
       this.userId = user.uid;
       this.books$ = this.bookservice.getBooks(this.userId);
     }
   )
  }

}
