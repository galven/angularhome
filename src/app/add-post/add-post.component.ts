import { PostFromFirestoreService } from './../post-from-firestore.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  btnText = "Add post";
  editMode: boolean = false;
  key: string;
  title:string;
  body:string;
  username:string;

  constructor(private router: Router,private pfFireStore: PostFromFirestoreService,private route: ActivatedRoute) { }

  onSubmit(){
    if(this.btnText){
      this.pfFireStore.updatePost(this.key,this.title,this.body,this.username)
    }else {
    this.pfFireStore.addNewPost(this.title,this.body,this.username);
    }
    this.router.navigate(['/post-from-firestore']);
  }

  ngOnInit() {

    this.key = this.route.snapshot.params.key;
    if(this.key) {
      this.editMode = true;
      this.btnText = "Update Post";
      this.pfFireStore.getSinglePost(this.key).subscribe(
    
        post => {
          this.title= post.data().title;
          this.body = post.data().body;
          this.username = post.data().username;
        }
  )}

  
  }

}
