import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authService:AuthService,
    private router:Router) {

      this.authService.LoginErrors().subscribe(error => {
        this.errorMessage = error;
      });
    }

    email:string;
    password:string; 
    errorMessage:String;

    onSubmit(){
      this.authService.signup(this.email,this.password);
      this.router.navigate(['/books']);
    }
  

  ngOnInit() {
  }

}
