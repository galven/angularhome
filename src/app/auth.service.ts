import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './interfaces/user';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable< User | null>
  private ErrorSubject = new Subject<string>();

  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState; // היוזר ינוהל ע"י פיירביס והמצב שלו יהיה כפי שהוא רשום בפיירבייס - יידע להגיד לנו אם היוזר מחובר או לא
  }

  signup(email:string, password:string) {
    this.afAuth
    .auth
    .createUserWithEmailAndPassword(email,password)
    .then(() => {
      this.router.navigate(['/books'])
    })
  }

  Logout(){
    this.afAuth.auth.signOut().then(res => console.log('sucessful logout!'))
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(() => this.router.navigate(['/books']).catch(error => window.alert(error)
        ))
  }

  LoginErrors():Subject<String> {
    return this.ErrorSubject;
  }

}
