import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BlogPost } from './interfaces/blog-post';

@Injectable({
  providedIn: 'root'
})
export class PostFromFirestoreService {

  private itemCollection: AngularFirestoreCollection<BlogPost>;
  private postsCollection: Observable<BlogPost[]>;

  constructor(private db: AngularFirestore) {

    this.itemCollection = db.collection<BlogPost>('posts');
   }

   getPostsFromFS():Observable<BlogPost[]>{
    this.postsCollection = this.itemCollection.valueChanges({idField: 'key'});
    console.log(this.postsCollection);
    return this.postsCollection;
}

getSinglePost(key:string){
  return this.db.collection('posts').doc(key).get();
}


addNewPost(title: String, body: String, username: String){
  this.db.collection('posts').add({
    title: title,
    body: body,
    username : username  
  })
}

updatePost(key:string, title: string, body: string, username: string){
  this.db.collection('posts').doc(key).update({
      title: title,
      body:body,
      username:username
  })
}

deletePost(key:string){
  this.db.collection('posts').doc(key).delete();
}

}
