import { BlogPost } from './../interfaces/blog-post';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { BlogUser } from '../interfaces/blog-user';
import { Router,UrlTree } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;

  Posts$: BlogPost[];
  Users$: BlogUser[];

  title:String;
  body:String;
  username:String;

  constructor(private Router:Router,private postservice:PostsService) { }

  ngOnInit() {

     this.postservice.getPost().subscribe(data =>this.Posts$ = data); 
     this.postservice.getUser().subscribe(data =>this.Users$ = data);
  }

  addAllPosts(){
   let postsArray = this.Posts$;
   let usersArray = this.Users$;
  this.postservice.addPosts(postsArray,usersArray);
  }


}
