import { Router } from '@angular/router';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  url:string;
  text:string;
  
  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classified']);
  }

  constructor(private classifyService:ClassifyService, 
    private router:Router,public imageService:ImageService) { }


 

  ngOnInit() {
  }

}
