import { Component, OnInit } from '@angular/core';
import { Router,UrlTree } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {


  // authors: object[] = [{id:1,author:'J.K.Rolling'},{id:2,author:'Yuval Noah Harri'}, {id:3, author:'Aeskol Nevo'},{id:4,author:'Eilan Hitner'},{id:5,author:'Asi Sharvit Gabay'}];
  author:string;
  id:number;

  onSubmit(){
    this.router.navigate(['/authors',this.id ,this.author]);
  }

  constructor(private router: Router) {

   }

  ngOnInit() {
     let url = this.router.url
     let urlArr = url.split('/')
    this.author = decodeURI(urlArr[3])
    this.id = parseInt(urlArr[2])
  
  }

}
