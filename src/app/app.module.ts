import { SignUpComponent } from './sign-up/sign-up.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { EditauthorComponent } from './editauthor/editauthor.component';
import { FormsModule }   from '@angular/forms';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts.service';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire'; // הוספתי
import { environment } from '../environments/environment'; // הוספתי
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AddBookComponent } from './add-book/add-book.component';
import { AddPostComponent } from './add-post/add-post.component';
import { PostFromFirestoreComponent } from './post-from-firestore/post-from-firestore.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import {MatCardModule} from '@angular/material/card';





const appRoutes: Routes = [
  { path: 'books', component: BooksComponent},
  {path: 'add-book', component:AddBookComponent},
  {path:'add-book/:id', component:AddBookComponent},
  { path: 'authors/:id/:author', component: AuthorsComponent},
  { path: 'editauthor/:id/:author', component: EditauthorComponent},
  { path: 'posts', component: PostsComponent},
  {path: 'add-post', component:AddPostComponent},
  {path: 'add-post/:key', component:AddPostComponent},
  {path: 'post-from-firestore', component:PostFromFirestoreComponent},
  {path: 'sign-up', component:SignUpComponent},
  {path: 'login', component:LoginComponent},
  {path: 'classify', component:DocformComponent},
  {path: 'classified', component:ClassifiedComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    AddBookComponent,
    AddPostComponent,
    PostFromFirestoreComponent,
    SignUpComponent,
    LoginComponent,
    DocformComponent,
    ClassifiedComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    AngularFireAuthModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig,'AngularHome'), // הוספתי עם השם של האפליקציה
    AngularFirestoreModule,
    AngularFireModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true })
  ],
  providers: [PostsService, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
