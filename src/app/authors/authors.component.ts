import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorsService } from './../authors.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {


 // typesOfAuthors: object[] = [{id: 1,author:'J.K.Rolling'},{id:2,author:'Yuval Noah Harri'}, {id:3, author:'Aeskol Nevo'},{id:4,author:'Eilan Hitner'},{id:5,author:'Asi Sharvit Gabay'}];
  
  constructor(private route: ActivatedRoute,private authorservice:AuthorsService) { }
  authors$:Observable<any>;

  author: any;
  id: any;
  addAuthor: any;

  onSubmit(){
    this.authorservice.addAuthors(this.addAuthor)
  }

  ngOnInit() {

    this.authors$ = this.authorservice.getAuthors();

    var that = this;
    this.author = this.route.snapshot.params.author;
    this.id = this.route.snapshot.params.id;

    this.authors$.forEach(function(obj){
      if(obj['id'] == that.id){
        obj['author'] = that.author;
      }
    });
  }

}
