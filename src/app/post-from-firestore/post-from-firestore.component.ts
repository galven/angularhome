import { PostFromFirestoreService } from './../post-from-firestore.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-from-firestore',
  templateUrl: './post-from-firestore.component.html',
  styleUrls: ['./post-from-firestore.component.css']
})
export class PostFromFirestoreComponent implements OnInit {

  panelOpenState = false;
  fsPosts$;

  constructor(private Router: Router, private pfFireStoreService: PostFromFirestoreService) { }

  delete(key: any) {
    this.pfFireStoreService.deletePost(key);
  }

  ngOnInit() {
    this.fsPosts$ = this.pfFireStoreService.getPostsFromFS();
    console.log(this.fsPosts$)
  }

}
