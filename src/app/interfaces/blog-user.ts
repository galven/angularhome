export interface BlogUser {

    id: number;
    name: string;
    username: string;
    email: string;
}
