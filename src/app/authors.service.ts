import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  typesOfAuthors: any = [{id: 1,author:'J.K.Rolling'},{id:2,author:'Yuval Noah Harri'}, {id:3, author:'Aeskol Nevo'},{id:4,author:'Eilan Hitner'},{id:5,author:'Asi Sharvit Gabay'}];
  arrLen: number = this.typesOfAuthors.length;

  addAuthors(authName: string){

    setInterval(() => 
      this.typesOfAuthors.push({id:this.typesOfAuthors.length + 1, author: authName}),5000); 
   
  }

  getAuthors(): any {
    const authorsObservable = new Observable(observer => {
           setInterval(() => {
             if(this.arrLen! = this.typesOfAuthors.length)
             observer.next(this.typesOfAuthors)}
, 5000);
    });
    return authorsObservable;


  }
}
